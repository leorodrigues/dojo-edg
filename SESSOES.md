# 01 AGO 2018

Participantes:

- Alexandre Paes
- Daniel Medina
- Iago Passos
- Leonardo Teixeira (Host)
- Luan Almeida
- Matheus Genteluci
- Vinícius Loureiro

## Retrospectiva

### Foi bom:
  - Engajamento e troca durante o pair programming
  - Os pilotos planejavam bastante antes de programar
  - Conseguimos terminar o desafio principal e iniciar o segundo
  - Foi legal usar o "mocha" para fazer os testes
  - Foi bom usar o VSCODE
  - Foi bom usar a tela do notebook com replicação, apesar de ficar menor
  - A taxa de commits foi boa
  - Programação ROOTS (sem debugger)

### Pode melhorar
  - Quebrar as modificações do software em itens menores (baby steps)
  - Projetar os cenários de teste antes de modificar o código implementado
  - Usar um teclado de desktop
  - Usar o notebook em português ou inglês
  - Interrupção para tentar "ligar" o debugger

# 02 JUL 2018

Participantes:

- André Carlos
- Iago Passos
- Leonardo Teixeira (host)

## Retrospectiva

### Foi bom:
  - O código resultante estava enxuto
  - Cobertura com teste de borda
  - Melhores descrições de commit
  - Uso do projetor de multimidia
  - Tempo melhor (mais rotação)

### Pode melhorar:
  - Baby step: investir mais tempo para descrever os próximos cenários num nível mais simples
  - Pedir mais ajuda: Usar timebox para tentar resolver um problema
  - Host tem que interferir menos: Comentar somente sobre técnicas e ferramentas.

# 22 JUN 2018

Participantes:

- André Carlos
- Iago Passos
- Leonardo Teixeira (host)
- Luiz Canet
- Monique Ferreira

## Retrospectiva

### Foi bom:
  - Aprender a usar o mocha
  - A experiência como um todo
  - Aprender algoritmos
  - Colaboração e troca de ideias
  - Engajamento dos participantes

### Pode melhorar:
  - Menos palpite do host
  - Tela maior (usar projetor)
