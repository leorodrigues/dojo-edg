const Cheque = require('../cheque');
const chai = require('chai');
const { expect } = chai;
describe('Testando cheque por extenso', () => {
    it('Deveria ver um texto vazio', function(){
        let valor = null;
        let textoEsperado = '';
        let cheque = new Cheque(valor);
        let valorPorExtenso = cheque.obterValorPorExtenso();
        expect(valorPorExtenso).to.be.equal(textoEsperado);
    });

    it('Deve retornar cheque invalido para chegue com valor 0', () => {
        let valor = 0;
        let textoEsperado = 'cheque invalido';
        let cheque = new Cheque(valor);
        let valorPorExtenso = cheque.obterValorPorExtenso();
        expect(valorPorExtenso).to.be.equal(textoEsperado);
    });

    it('Deve retornar um centavo para chegue com valor 0.01', () => {
        let valor = 0.01;
        let textoEsperado = 'um centavo';
        let cheque = new Cheque(valor);
        let valorPorExtenso = cheque.obterValorPorExtenso();
        expect(valorPorExtenso).to.be.equal(textoEsperado);
    });

    it('Deve verificar se o tipo passado eh um numero', () => {
        let valor = 'oi';
        let textoEsperado = 'O valor deve ser um numero';
        let cheque = new Cheque(valor);
        let valorPorExtenso = cheque.obterValorPorExtenso();
        expect(valorPorExtenso).to.be.equal(textoEsperado);
    });
});