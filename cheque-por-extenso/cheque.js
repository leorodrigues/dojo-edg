class Cheque {

    constructor (valor) {
        this.valor = valor;
    }
    obterValorPorExtenso () {
        if (this.valor == null) {
            return '';
        }
        if (this.valor === 0){
            return 'cheque invalido';
        }
        if(this.valor === 0.01){
            return 'um centavo';
        }
        if(!(this.valor instanceof Number)){
            return 'O valor deve ser um numero';
        }
    }
}

module.exports = Cheque;
