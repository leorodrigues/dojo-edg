const chai = require('chai');
const expect = chai.expect;
const calculaTroco = require('../index');
describe('computar um troco', function() {
    it('Deveria retornar zero', function() {
        let resultado = calculaTroco(5,5);
        expect(resultado).to.be.deep.equals([]);
    });
    it('Retorna uma nota de 1', function() {
        let resultado = calculaTroco(9,10);
        expect(resultado).to.be.deep.equals([1]);
    });
    it('Retorna uma nota de 2', () => {
        let resultado = calculaTroco(8,10);
        expect(resultado).to.be.deep.equals([2]);
    });
    it('Retorna uma nota de 5', () => {
        let resultado = calculaTroco(5,10);
        expect(resultado).to.be.deep.equals([5]);
    });
    it('Retorna uma nota de 10', () => {
        let resultado = calculaTroco(10,20);
        expect(resultado).to.be.deep.equals([10]);
    });

    it('Retorna troco com duas notas de 2', () => {
        let resultado = calculaTroco(6,10);
        expect(resultado).to.be.deep.equals([2,2]);
    });

    it('Retorna troco com uma nota de 1 e uma de 2', () => {
        let resultado = calculaTroco(7,10);
        expect(resultado).to.be.deep.equals([1,2]);
    });

    it('Retorna troco de 6 reais', () => {
        let resultado = calculaTroco(4,10);
        expect(resultado).to.be.deep.equals([1,5]);
    });

    it('Retorna troco de 7 reais', () => {
        let resultado = calculaTroco(3,10);
        expect(resultado).to.be.deep.equals([2,5]);
    });

    it('Retorna troco de 8 reais', () => {
        let resultado = calculaTroco(2,10);
        expect(resultado).to.be.deep.equals([1,2,5]);
    });

    it('Retorna troco de 9 reais', () => {
        let resultado = calculaTroco(1,10);
        expect(resultado).to.be.deep.equals([2,2,5]);
    });

    it('Retorna troco de 44 reais', () => {
        let resultado = calculaTroco(6,50);
        expect(resultado).to.be.deep.equals([2,2,10,10,10,10]);
    });

    it('Retorna troco de 0.75', () => {
        let resultado = calculaTroco(0.25,1);
        expect(resultado).to.be.deep.equals([0.25, 0.5]);
    });

     it('Retorna troco 0.01', () => {
        let resultado = calculaTroco(0.04, 0.05);
        expect(resultado).to.be.deep.equals([0.01]);
    });

    it('Retorna troco 0.10', () => {
        let resultado = calculaTroco(0.9, 1);
        expect(resultado).to.be.deep.equals([0.1]);
    });

    it('Retorna troco 0.25', () => {
        let resultado = calculaTroco(0.75, 1);
        expect(resultado).to.be.deep.equals([0.25]);
    });

    it('Retorna troco 0.5', () => {
        let resultado = calculaTroco(0.5, 1);
        expect(resultado).to.be.deep.equals([0.5]);
    });

    it('Retorna troco 0.05', () => {
        let resultado = calculaTroco(0.95, 1);
        expect(resultado).to.be.deep.equals([0.05]);
    });

    it('Retorna troco 1.05', () => {
        let resultado = calculaTroco(0.95, 2);
        expect(resultado).to.be.deep.equals([0.05, 1]);
    });

    it('Retorna troco 15.05', () => {
        let resultado = calculaTroco(0.95, 16);
        expect(resultado).to.be.deep.equals([0.05, 5, 10]);
    });

    it('Retorna troco 11.20', () => {
        let resultado = calculaTroco(0.80, 12);
        expect(resultado).to.be.deep.equals([0.1, 0.1, 1, 10]);
    });

    it('Dar exception caso não tenha troco', () => {
        expect(() => calculaTroco(1, 10,[])).to.throw(Error);
    });

    it('Retorna troco de 1 real', () => {
        let resultado = calculaTroco(1, 2, [0,0,0,0,0,1,0,0,0]);
        expect(resultado).to.be.deep.equals([1]);
    });

    it('Retorna troco de R$3 sem notas de 2 reais', () => {
        let resultado = calculaTroco(2, 5, [0,0,0,0,0,3,0,0,0]);
        expect(resultado).to.be.deep.equals([1, 1, 1]);
    });

    it('Deveria retornar o troco de 3 reais mas só possui uma nota de 5', () => {
        expect(() => calculaTroco(2, 5, [0,0,0,0,0,0,0,1,0])).to.throw(Error);
    });

    it('Deveria retornar 5 reais mas só tem uma nota de 10 e lança Exception', () => {
        expect(() => calculaTroco(15, 20, [0,0,0,0,0,0,0,0,1])).to.throw(Error);
    });

    it('Deveria retornar Exception, mas como possui nota menor, aceita o valor', () => {
        let disponivelEmCash = [0,0,0,0,0,0,0,0,1];
        expect(() => calculaTroco(5, 10, disponivelEmCash)).to.throw(Error);
        expect(calculaTroco(5, 15, disponivelEmCash)).to.be.deep.equals([10]);
    });
})
