function comparaCrescente(a, b) {
    return a - b;
}
function calculaTroco(valProduto, valPago, disponivelEmCash=[100,100,100,100,100,100,100,100,100]) {
    var notasPossiveis = [0.01, 0.05, 0.1, 0.25, 0.5, 1, 2, 5, 10];
    var listadeNotas = [];
    var troco = (valPago - valProduto).toFixed(2);
    var i = notasPossiveis.length - 1;

    let soma = 0;

    for (let index = 0; index < notasPossiveis.length; index++) {
         soma += notasPossiveis[index] * (disponivelEmCash[index] || 0);
    }

    if(soma < troco) throw new Error('Não possui troco suficiente!');

    while(troco > 0.0){
       if (troco >= notasPossiveis[i] && (disponivelEmCash[i] > 0)) {
        listadeNotas.push(notasPossiveis[i]);
        troco = (troco - notasPossiveis[i]).toFixed(2);
       } else {
        i--;
        if(i < 0) throw new Error('Transação não concluida');
       }
    }
    return listadeNotas.sort(comparaCrescente);
}

module.exports = calculaTroco;
